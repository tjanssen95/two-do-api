<?php
// Routes
$app->get('/', function ($request, $response, $args) {
	$sth = $this->db->prepare("SELECT * FROM todos ORDER BY date_updated DESC");
	$sth->execute();
	$todos = $sth->fetchAll();

	return $this->view->render($response, 'base.html', ['todos' => $todos]);
});

$app->get('/add', function ($request, $response, $args) {
	return $this->view->render($response, 'add.html');
});

$app->get('/todos', function ($request, $response, $args) {
	$sth = $this->db->prepare("SELECT * FROM todos ORDER BY date_updated DESC");
	$sth->execute();
	$todos = $sth->fetchAll();

	return $this->response->withJson($todos);
});

$app->get('/todo/[{id}]', function ($request, $response, $args) {
	$sth = $this->db->prepare("SELECT * FROM todos WHERE id=:id");
	$sth->bindParam("id", $args['id']);
	$sth->execute();
	$todo = $sth->fetchObject();

	return $this->response->withJson($todo);
});

$app->get('/todos/search/[{query}]', function ($request, $response, $args) {
	$sth = $this->db->prepare("SELECT * FROM todos WHERE UPPER(title) LIKE :query ORDER BY date_created DESC");
	$query = "%" . $args['query'] . "%";
	$sth->bindParam('query', $query);
	$sth->execute();
	$todos = $sth->fetchAll();

	return $this->response->withJson($todos);
});

$app->post('/todo', function ($request, $response) {
	$input = $request->getParsedBody();
	$sql = "INSERT INTO todos (todo) VALUES (:todo)";
	$sth = $this->db->prepare($sql);
	$sth->bindParam('todo', $input['todo']);
	$sth->execute();
	$input['id'] = $this->db->lastInsertId();

	return $this->response->withJson($input);
});

$app->delete('/todo/[{id}]', function ($request, $response, $args) {
    $sth = $this->db->prepare("DELETE FROM todos WHERE id=:id");
    $sth->bindParam("id", $args['id']);
    $sth->execute();
    $todos = $sth->fetchAll();

    return $app->redirect('/');
});

$app->put('/todo/[{id}]', function ($request, $response, $args) {
	$input = $request->getParsedBody();
	$sql = "UPDATE todos SET todo=:todo WHERE id=:id";
	$sth = $this->db->prepare($sql);
	$sth->bindParam('id', $args['id']);
	$sth->bindParam('todo', $args['todo']);
	$sth->execute();
	$input['id'] = $args['id'];

	return $this->response->withJson($input);
});